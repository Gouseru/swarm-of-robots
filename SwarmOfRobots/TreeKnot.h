#pragma once
#include<assert.h>
#include<list>
using std::list;

#define N 3

template <typename T>
class TreeKnot
{
	friend class Swarm;

private:
	double* key;
	// �������� ��������� �� ����������
	std::shared_ptr<TreeKnot<T>> more;
	std::shared_ptr<TreeKnot<T>> less;
	// ������ ��������
	list<T> objects;
	// ���-�� ��������
	int size;

public:
	// �����������
	TreeKnot() {
		key = nullptr;
		more = nullptr;
		less = nullptr;
		objects;
		size = 0;
	}

	// �������� �������� �����
	// \return ����
	const double* get_key() const {
		return key;
	}

	// �������� ������ �� ��������� � ������� �������� �����
	// \return ������
	const TreeKnot<T>* get_more() const {
		return more.get();
	}

	// �������� ������ �� ��������� � ������� �������� �����
	// \return ������
	const TreeKnot<T>* get_less() const {
		return less.get();
	}

	// �������� ������ �� ������ ��������
	// \return ������
	const list<T>& get_objects() const {
		return objects;
	}

	// �������� ������ �� ������ ��������
	// \return �����
	int get_size() const {
		return size;
	}

	// ��������� ������ � ������
	// ����� ���������� �����
	// \new_object - ������ �� ����� ������
	void add_object(const T& new_object) {
		add_object_to_leaf_x(new_object);
		size++;
	}

	// ��������� ������ � ����
	// ��� ������������ ������� ������� ����� ������
	// ������� ���������� �� ��� x
	// \new_object - ������ �� ����� ������
	// \return 0 ��� �����
	// \return 1 ���� ���� �� �������� ������
	void add_object_to_leaf_x(const T& new_object) {
		// ���� �� ����, �� ������ �� ������
		if (key != nullptr) {
			if (new_object.get_position_x() > * key)
				more->add_object_to_leaf_y(new_object);
			else
				less->add_object_to_leaf_y(new_object);
			return;
		}
		objects.push_back(new_object);

		// ���� �������� �����, �� ����� ���� �� 2 �����
		if (objects.size() == N + 1)
			share_leaf_x();
	}

	// ��������� ������ � ����
	// ��� ������������ ������� ������� ����� ������
	// ������� ���������� �� ��� y
	// \new_object - ������ �� ����� ������
	// \return 0 ��� �����
	// \return 1 ���� ���� �� �������� ������
	void add_object_to_leaf_y(const T& new_object) {
		// ���� �� ����, �� ������ �� ������
		if (key != nullptr) {
			if (new_object.get_position_y() > * key)
				more->add_object_to_leaf_x(new_object);
			else
				less->add_object_to_leaf_x(new_object);
			return;
		}
		objects.push_back(new_object);

		// ���� �������� �����, �� ����� ���� �� 2 �����
		if (objects.size() == N + 1)
			share_leaf_y();
	}

	// ������ ���� �� 2 ���� �� x
	void share_leaf_x() {
		// �������� ������ �� ����� ������
		less = std::make_shared<TreeKnot<T>>();
		more = std::make_shared<TreeKnot<T>>();

		// ��������� ������, ����� ����� ������
		sort_leaf_x();

		auto it = objects.begin();
		int i = 0;
		double x1, x2;

		// ��������� �������� �������� � ���� �� ��������� ������ ����� 
		for (; i < (N + 1) / 2; it = objects.begin(), i++)
		{
			less->add_object_to_leaf_y(*it);
			x1 = (*it).get_position_x();
			objects.erase(it);
		};

		// �������� ����
		x2 = (*it).get_position_x();
		key = new double((x1 + x2) / 2);

		// ��������� �������� �������� � ���� �� ��������� ������ ����� 
		for (; i < (N + 1); it = objects.begin(), i++)
		{
			more->add_object_to_leaf_y(*it);
			objects.erase(it);
		};
	}
	
	// ������ ���� �� 2 ���� �� y
	void share_leaf_y() {
		// �������� ������ �� ����� ������
		less = std::make_shared<TreeKnot<T>>();
		more = std::make_shared<TreeKnot<T>>();

		// ��������� ������, ����� ����� ������
		sort_leaf_x();

		auto it = objects.begin();
		int i = 0;
		double x1, x2;

		// ��������� �������� �������� � ���� �� ��������� ������ ����� 
		for (; i < (N + 1) / 2; it = objects.begin(), i++)
		{
			less->add_object_to_leaf_x(*it);
			x1 = (*it).get_position_y();
			objects.erase(it);
		};

		// �������� ����
		x2 = (*it).get_position_y();
		key = new double((x1 + x2) / 2);

		// ��������� �������� �������� � ���� �� ��������� ������ ����� 
		for (; i < (N + 1); it = objects.begin(), i++)
		{
			more->add_object_to_leaf_x(*it);
			objects.erase(it);
		};
	}

	// ��������� ������� ����� �� x
	void sort_leaf_x() {
		objects.sort([](const T& a, const T& b) -> bool {
			return a.get_position_x() < b.get_position_x();
		});
	}

	// ��������� ������� ����� �� y
	void sort_leaf_y() {
		objects.sort([](const T& a, const T& b) -> bool {
			return a.get_position_y() < b.get_position_y();
		});
	}
	
	// ���� �������� � ������
	list<T*> get_objects(double x1, double y1, double x2, double y2, int n) {
		return get_objects_x(x1, y1, x2, y2, n);
	}

	// ���� �������� � �����
	list<T*> get_objects_x(double x1, double y1, double x2, double y2, int n) {
		list<T*> res;
		if (key == nullptr)
			res = search_objects(x1, y1, x2, y2);
		else {
			if (*key > fmax(x1, x2))
				res = more->get_objects_y(x1, y1, x2, y2, n);
			else
				if (*key < fmin(x1, x2))
					res = less->get_objects_y(x1, y1, x2, y2, n);
				else {
					res = more->get_objects_y(x1, y1, x2, y2, n);
					list<T*> res1 = less->get_objects_y(x1, y1, x2, y2, n);
					res.insert(res.end(), res1.begin(), res1.end());
				}
		}
		if (res.size() > (unsigned)n)
			res.resize(n);
		return res;
	}
	
	// ���� �������� � �����
	list<T*> get_objects_y(double x1, double y1, double x2, double y2, int n) {
		list<T*> res;
		if (key == nullptr)
			res = search_objects(x1, y1, x2, y2);
		else {
			if (*key > fmax(y1, y2))
				res = more->get_objects_x(x1, y1, x2, y2, n);
			else
				if (*key < fmin(y1, y2))
					res = less->get_objects_x(x1, y1, x2, y2, n);
				else {
					res = more->get_objects_x(x1, y1, x2, y2, n);
					list<T*> res1 = less->get_objects_x(x1, y1, x2, y2, n);
					res.insert(res.end(), res1.begin(), res1.end());
				}
		}
		if (res.size() > (unsigned)n)
			res.resize(n);
		return res;
	}

	// ���� �������� � ������
	list<T*> search_objects(double x1, double y1, double x2, double y2) {
		list<T*> res;
		// c������� �������� �� ������ ������� ������
		auto it = objects.begin();
		// ���� �������� �� ����� �� ������� ������
		while (it != objects.end()) {
			if (it->check_position(x1, y1, x2, y2) && it->empty_commander()) {
				res.push_back(&(*it));
			}
			++it;
		}
		return res;
	}

	// ������ � �������
	void print() {
		if (key == nullptr) {
			// c������� �������� �� ������ ������� ������
			auto it = objects.begin();
			// ���� �������� �� ����� �� ������� ������
			while (it != objects.end()) {
				it->print();
				++it;
			}
		}
		else {
			more->print();
			less->print();
		}
	}

};

