#pragma once

#include "Robots.h"

#ifdef PROJECT_EXPORTS
#define PROJECT_API __declspec(dllexport)
#else
#define PROJECT_API __declspec(dllimport)
#endif

extern "C" {
		
	// ������� ���
	PROJECT_API Swarm* create_swarm();

	// ������� ���
	PROJECT_API void delete_swarm(Swarm* swarm);

	// ��������� ������ � ��� � ������������� �� ���������
	PROJECT_API void swarm_add_robot_common(Swarm* swarm);

	// ��������� ������ ��� ����� � ���
	PROJECT_API void swarm_add_robot(Swarm* swarm, int position_x, int position_y, int rotation);

	// ��������� ������ � ���
	PROJECT_API void swarm_add_robot_name(Swarm* swarm, char* name, int position_x, int position_y,
		int rotation);

	// ��������� ������-��������� � ��� � ������������� �� ���������
	PROJECT_API void swarm_add_commander_common(Swarm* swarm);

	// ��������� ������-��������� ��� ����� � ���
	PROJECT_API void swarm_add_commander(Swarm* swarm, int position_x, int position_y, int rotation);

	// ��������� ������-��������� � ���
	PROJECT_API void swarm_add_commander_name(Swarm* swarm, char* name, int position_x, int position_y,
		int rotation);

	// ��������� �������� ��������
	PROJECT_API void swarm_add_scenario(Swarm* swarm, Scenario* scen);

	// ��������� ��������
	PROJECT_API void swarm_do_scenario(Swarm* swarm);

	// �����������
	PROJECT_API Scenario* create_scenario();

	// ������� ��������
	PROJECT_API void delete_scenario(Scenario* scenario);

	// �������� ������� ��� �����
	// ���������� ��������� ��������
	PROJECT_API void scenario_add_task(Scenario* scenario, int lead_time, int x1, int y1,
		int x2, int y2, int number_of_robots);

	// �������� �������
	// ���������� ��������� ��������
	PROJECT_API void scenario_add_task_name(Scenario* scenario, char* name, int lead_time,
		int x1, int y1, int x2, int y2, int number_of_robots);

	// �������� ��������
	PROJECT_API Swarm::iterator<RobotWorker>* swarm_create_iterator_worker(Swarm* swarm);
	
	// �������� ��������
	PROJECT_API Swarm::iterator<RobotCommander>* swarm_create_iterator_comm(Swarm* swarm);

	// ��������� ����� �� ������
	PROJECT_API int swarm_iterator_is_empty_worker(Swarm::iterator<RobotWorker>* iterator);

	// ��������� ����� �� ������
	PROJECT_API int swarm_iterator_is_empty_comm(Swarm::iterator<RobotCommander>* iterator);

	// �������� ������
	PROJECT_API RobotWorker* swarm_iterator_get_worker(Swarm::iterator<RobotWorker>* iterator);

	// �������� ������
	PROJECT_API RobotCommander* swarm_iterator_get_comm(Swarm::iterator<RobotCommander>* iterator);

	// �������� ������
	PROJECT_API void swarm_iterator_next_worker(Swarm::iterator<RobotWorker>* iterator);

	// �������� ������
	PROJECT_API void swarm_iterator_next_comm(Swarm::iterator<RobotCommander>* iterator);

	// �������� ��� ������
	PROJECT_API void robot_get_name(RobotWorker* rob, char* name, int max_name_len);

	// �������� ���������� x ������
	PROJECT_API int robot_get_x(RobotWorker* rob);

	// �������� ���������� y ������
	PROJECT_API int robot_get_y(RobotWorker* rob);

	// �������� ���� �������� ������
	PROJECT_API int robot_get_rotation(RobotWorker* rob);

	//
	PROJECT_API void robot_print(RobotWorker* rob);

	// �������� ��������
	PROJECT_API Scenario::iterator* scenario_create_iterator(Scenario* scen);

	// ��������� ����� �� ������
	PROJECT_API int scenario_iterator_is_empty(Scenario::iterator* iterator);

	// �������� �������
	PROJECT_API Task* scenario_iterator_get_task(Scenario::iterator* iterator);

	// �������� ����� ����������
	PROJECT_API int scenario_iterator_get_result(Scenario::iterator* iterator);

	// �������� ��������� �� 1
	PROJECT_API void scenario_iterator_next(Scenario::iterator* iterator);

	// �������� ���������� x ������
	PROJECT_API void scenario_explain_result(Scenario* scen, int n, char* str, int max_str_len);

	// �������� ��� ������
	PROJECT_API void task_get_name(Task* task, char* name, int max_name_len);

	// �������� ���������� x1
	PROJECT_API int task_get_x1(Task* task);

	// �������� ���������� y1
	PROJECT_API int task_get_y1(Task* task);

	// �������� ���������� x2
	PROJECT_API int task_get_x2(Task* task);

	// �������� ���������� y2
	PROJECT_API int task_get_y2(Task* task);

	// �������� ����� ����������
	PROJECT_API int task_get_lead_time(Task* task);

	// �������� ��������� ����������� �������
	PROJECT_API int task_get_number_of_robots(Task* task);
}