#include <assert.h>
#include "Scenario.h"


Scenario::Scenario() {
	tasks;
	result;
	time;
}

void Scenario::add_task(const Task& new_task) {
	tasks.push_back(new_task);
	result.push_back(0);
	time.push_back(0);
}

void Scenario::add_task(int lead_time, double x1, double y1, double x2, double y2, 
	int number_of_robots) {
	tasks.push_back(Task(lead_time, x1, y1, x2, y2, number_of_robots));
	result.push_back(0);
	time.push_back(0);
}

void Scenario::add_task(int lead_time, double x1, double y1, double x2, double y2, 
	int number_of_robots, RobotCommander* commander) {
	tasks.push_back(Task(lead_time, x1, y1, x2, y2, number_of_robots, commander));
	result.push_back(0);
	time.push_back(0);
}

void Scenario::add_task(const std::string& name, int lead_time, double x1, double y1, double x2, double y2, 
	int number_of_robots) {
	tasks.push_back(Task(name, lead_time, x1, y1, x2, y2, number_of_robots));
	result.push_back(0);
	time.push_back(0);
}

void Scenario::add_task(const std::string& name, int lead_time, double x1, double y1, double x2, double y2,
	int number_of_robots, RobotCommander* commander) {
	tasks.push_back(Task(name, lead_time, x1, y1, x2, y2, number_of_robots, commander));
	result.push_back(0);
	time.push_back(0);
}

const list<Task>& Scenario::get_task() const {
	return tasks;
}

Task* Scenario::get_task(int n) {
	auto it = tasks.begin();
	int i = 0;
	for (; it != tasks.end() && i < n; it++, i++);
	if ((it != tasks.end()) && (i == n))
		return &(*it);
	else
		return nullptr;
}

int Scenario::get_lead_time(int n) const {
	auto it = tasks.begin();
	int i = 0;
	for (; (it != tasks.end()) && (i < n); it++, i++);
	if ((it != tasks.end()) && (i == n))
		return it->get_lead_time();
	else
		return -1;
}

const list<int>& Scenario::get_result() const {
	return result;
}

int Scenario::get_result(int n) const {
	auto it = result.begin();
	int i = 0;
	for (; (it != result.end()) && (i < n); it++, i++);
	if ((it != result.end()) && (i == n))
		return *it;
	else
		return -1;
}

void Scenario::set_result(int n, int res){
	auto it = result.begin();
	int i = 0;
	for (; (it != result.end()) && (i < n); it++, i++);
	if ((it != result.end()) && (i == n))
		*it = res;
}

void Scenario::clear_result() {
	for (auto it = result.begin(); it != result.end(); it++) {
		*it = 0;
	}
}

string Scenario::explain_result(int res) {
	switch (res) {
	case 0:
		return (string)("Task not started");
	case 1:
		return (string)("Task in progress");
	case 2:
		return (string)("Task delayed");
	case 3:
		return (string)("Task in progress after delay");
	case 4:
		return (string)("Task canceled");
	case 5:
		return (string)("Task completed");
	case 6:
		return (string)("Task completed with a delay");
	default:
		return (string)("Unknown result");
	}
}

const list<int>& Scenario::get_time() const {
	return time;
}

int Scenario::get_time(int n) const {
	auto it = time.begin();
	int i = 0;
	for (; (it != time.end()) && (i < n); it++, i++);
	if ((it != time.end()) && (i == n))
		return *it;
	else
		return -1;
}

void Scenario::set_time(int n, int res) {
	auto it = time.begin();
	int i = 0;
	for (; (it != time.end()) && (i < n); it++, i++);
	if ((it != time.end()) && (i == n))
		*it = res;
}

void Scenario::clear_time() {
	for (auto it = time.begin(); it != time.end(); it++) {
		*it = 0;
	}
}
