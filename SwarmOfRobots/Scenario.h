#pragma once
#include <list>
#include <assert.h>
#include "Task.h"

using std::list;

class Scenario
{
private:
	// ������ �������
	list<Task> tasks;
	// ������ �������, ������� ������ � ������ ���������� �������
	// �� �� ������, ��� ���������� ��� �������
	list<int> time;
	// ������ ����������� ���������� �������
	list<int> result;
	

public:
	// �����������
	Scenario();

	// �������� �������
	void add_task(const Task& new_task);

	// �������� ������� ��� �����
	// ���������� ��������� ��������
	void add_task(int lead_time, double x1, double y1, double x2, double y2, int number_of_robots);

	// �������� ������� ��� �����
	void add_task(int lead_time, double x1, double y1, double x2, double y2, int number_of_robots,
		RobotCommander* commander);
	
	// �������� �������
	// ���������� ��������� ��������
	void add_task(const std::string& name, int lead_time, double x1, double y1, double x2, double y2,
		int number_of_robots);

	// �������� �������
	void add_task(const std::string& name, int lead_time, double x1, double y1, double x2, double y2,
		int number_of_robots, RobotCommander* commander);

	// �������� ������ �� ������ �������
	const list<Task>& get_task() const;

	// �������� ������ �� ������� �� ������
	// ���� ������� ������ ��� ��������� ����� �� ������������ ������ �� ��������� �������
	// \res ������ �� �������
	Task* get_task(int n);

	// �������� ��������� �� ������
	// ���� ������� ������ ��� ��������� ����� �� ������������ ��������� ���������� �������
	// \res �����
	// \res -1 ���� ������ ���������� ���
	int get_lead_time(int n) const;

	// ���������� ������ ����������� ������� ��������
	// \return ������ ��� ������� ��������(������ ����, ��� ��������)
	const list<int>& get_result() const;

	// �������� ��������� �� ������
	// ���� ������� ������ ��� ��������� ����� �� ������������ ��������� ���������� �������
	// \res �����
	// \res -1 ���� ������ ���������� ���
	int get_result(int n) const;

	// ������ ��������� �� ������
	// ���� ������� ������ ��� ��������� ����� �� ������ �� ����������
	void set_result(int n, int res);

	// ������� ������ ����������� �������
	void clear_result();

	// ���������� ������ � ��������� ���������� �������
	// \res 0 ������� �� ������
	// \res 1 ������� � �������� ����������
	// \res 2 ������� ���������
	// \res 3 ������� � �������� ���������� ����� ��������
	// \res 4 ������� ��������
	// \res 5 ������� ���������
	// \res 6 ������� ��������� � ���������
	// \return ������
	static string explain_result(int res);

	// ���������� ������ ����������� ������� ��������
	// \return ������ ��� ������� ��������(������ ����, ��� ��������)
	const list<int>& get_time() const;

	// �������� ��������� �� ������
	// ���� ������� ������ ��� ��������� ����� �� ������������ ��������� ���������� �������
	// \res �����
	// \res -1 ���� ������ ���������� ���
	int get_time(int n) const;

	// ������ ��������� �� ������
	// ���� ������� ������ ��� ��������� ����� �� ������ �� ����������
	void set_time(int n, int res);

	// ������� ������ ����������� �������
	void clear_time();

	// ����� ��������� ��� ��������� �������� � �������� ������ �������
	class iterator {
		// ������ �������
		list<Task>* tasks;
		// ������ ����������� ���������� �������
		list<int>* result;
		// ���������, �� �������� ����������� ������������
		Task* gm;
		int* gmres;

	public:
		// �����������; �������������� �������� ���, �����
		// ����� ��� ������� ����� ����� ���� ��������� ������
		iterator(list<Task>& tasks, list<int>& result) {
			this->tasks = &tasks;
			this->result = &result;
			auto it = this->tasks->begin();
			auto itres = this->result->begin();
			if (it == this->tasks->end()) {
				gm = nullptr;
				gmres = nullptr;
			}
			else {
				gm = &(*it);
				gmres = &(*itres);
			}
		}

		iterator(list<Task>& tasks, list<int>& result, Task& task, int& res) {
			this->tasks = &tasks;
			this->result = &result;
			gm = &task;
			gmres = &res;
		}

		bool is_empty() const {
			auto it = tasks->end();
			return (gm == nullptr);
		}

		Task* operator*() {
			// ���� ������ �������� ���, �������� ����������.
			if (is_empty()) {
				char buf[50];
				sprintf_s(buf, "No more tasks in scenario");
				throw std::out_of_range(buf);
			}
			// ������� �������� �� ���������� �������
			return gm;
		}

		int get_result() {
			// ���� ������ �������� ���, �������� ����������.
			if (is_empty()) {
				char buf[50];
				sprintf_s(buf, "No more tasks in scenario");
				throw std::out_of_range(buf);
			}
			// ������� �������� �� ���������� �������
			return *gmres;
		}

		iterator& operator++() {
			if (!is_empty()) {
				// c������� �������� �� ������ ������� ������
				auto it = tasks->begin();
				auto itres = this->result->begin();
				// ���� �������� �� ����� �� ������� ������
				while (it != tasks->end()) {
					if (&(*it) == gm) {
						++it;
						++itres;
						break;
					}
					++it;
					++itres;
				}
				// ���� ����� ������ ��������� � �� �� ��������� � ������, �� ������� ��� �� ���������
				if (it != tasks->end()) {
					gm = &(*it);
					gmres = &(*itres);
				}
				else {
					gm = nullptr;
					gmres = nullptr;
				}
			}
			return *this;
		}
	};

	// ��������� ��������� ��� ������� �� ���� �������
	// \return ��������
	iterator* take_iterator_scenario() {
		return new Scenario::iterator(tasks, result);
	}
};

