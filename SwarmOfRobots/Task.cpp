#include <iostream>
#include "Task.h"

Task::Task() {
	this->name = "UnknownTask";
	this->lead_time = 0;
	this->x1 = 0;
	this->y1 = 0;
	this->x2 = 0;
	this->y2 = 0;
	this->number_of_robots = 0;
	this->commander = nullptr;
}

Task::Task(int lead_time, double x1, double y1, double x2, double y2, int number_of_robots) {
	this->name = "UnknownTask";
	if (lead_time < 0) {
		char buf[50];
		sprintf_s(buf, "Can't create task with lead time: %i", lead_time);
		throw std::invalid_argument(buf);
	}
	this->lead_time = lead_time;
	this->x1 = x1;
	this->y1 = y1;
	this->x2 = x2;
	this->y2 = y2;
	if (number_of_robots < 0) {
		char buf[50];
		sprintf_s(buf, "Can't create task with number of robots: %i", number_of_robots);
		throw std::invalid_argument(buf);
	}
	this->number_of_robots = number_of_robots;
	this->commander = nullptr;
}

Task::Task(int lead_time, double x1, double y1, double x2, double y2, int number_of_robots, RobotCommander* commander) {
	this->name = "UnknownTask";
	if (lead_time < 0) {
		char buf[50];
		sprintf_s(buf, "Can't create task with lead time: %i", lead_time);
		throw std::invalid_argument(buf);
	}
	this->lead_time = lead_time;
	this->x1 = x1;
	this->y1 = y1;
	this->x2 = x2;
	this->y2 = y2;
	if (number_of_robots < 0) {
		char buf[50];
		sprintf_s(buf, "Can't create task with number of robots: %i", number_of_robots);
		throw std::invalid_argument(buf);
	}
	this->number_of_robots = number_of_robots;
	this->commander = commander;
}

Task::Task(const std::string& name, int lead_time, double x1, double y1, double x2, double y2, 
			int number_of_robots) {
	this->name = name;
	if (lead_time < 0) {
		char buf[50];
		sprintf_s(buf, "Can't create task with lead time: %i", lead_time);
		throw std::invalid_argument(buf);
	}
	this->lead_time = lead_time;
	this->x1 = x1;
	this->y1 = y1;
	this->x2 = x2;
	this->y2 = y2;
	if (number_of_robots < 0) {
		char buf[50];
		sprintf_s(buf, "Can't create task with number of robots: %i", number_of_robots);
		throw std::invalid_argument(buf);
	}
	this->number_of_robots = number_of_robots;
	this->commander = nullptr;
}

Task::Task(const std::string& name, int lead_time, double x1, double y1, double x2, double y2, 
			int number_of_robots, RobotCommander* commander) {
	this->name = name;
	if (lead_time < 0) {
		char buf[50];
		sprintf_s(buf, "Can't create task with lead time: %i", lead_time);
		throw std::invalid_argument(buf);
	}
	this->lead_time = lead_time;
	this->x1 = x1;
	this->y1 = y1;
	this->x2 = x2;
	this->y2 = y2;
	if (number_of_robots < 0) {
		char buf[50];
		sprintf_s(buf, "Can't create task with number of robots: %i", number_of_robots);
		throw std::invalid_argument(buf);
	}
	this->number_of_robots = number_of_robots;
	this->commander = commander;
}

const std::string& Task::get_name() const {
	return name;
}

int Task::get_lead_time() const {
	return lead_time;
}

double Task::get_x1() const {
	return x1;
}

double Task::get_y1() const {
	return y1;
}

double Task::get_x2() const {
	return x2;
}

double Task::get_y2() const {
	return y2;
}

RobotCommander& Task::get_commander() {
	return *commander;
}

void Task::set_commander(RobotCommander& new_comm) {
	commander = &new_comm;
}

int Task::get_number_of_robots() const {
	return number_of_robots;
}
