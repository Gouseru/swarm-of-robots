#include "RobotCommander.h"

double RobotCommander::random(double min, double max)
{
	return (double)(rand()) / RAND_MAX * (max - min) + min;
}

RobotCommander::RobotCommander()
	: RobotWorker::RobotWorker()
{
	subordinates;
}

RobotCommander::RobotCommander(double position_x, double position_y, double rotation)
	: RobotWorker::RobotWorker(position_x, position_y, rotation)
{
	subordinates;
}

RobotCommander::RobotCommander(const string& name, double position_x, double position_y, double rotation)
	: RobotWorker::RobotWorker(name, position_x, position_y, rotation)
{
	subordinates;
}

void RobotCommander::set_subordinates(list<RobotWorker*> new_subordinates) {
	subordinates = new_subordinates;

	auto it = subordinates.begin();
	// ���� �������� �� ����� �� ������� ������
	while (it != subordinates.end()) {
		(*it)->set_commander(*this);
		++it;
	}
	this->set_commander(*this);
}

list<RobotWorker*> RobotCommander::get_subordinates() {
	return subordinates;
}

bool RobotCommander::check_position_comm(double check_position_x1, double check_position_y1,
	double check_position_x2, double check_position_y2) {

	for (auto it = subordinates.begin(); it != subordinates.end(); ++it) {
		if (!((*it)->check_position(check_position_x1, check_position_y1, 
									check_position_x2, check_position_y2)))
			return false;
	}
	return check_position(check_position_x1, check_position_y1,
						  check_position_x2, check_position_y2);
}

void RobotCommander::move_to_comm(double x1, double y1, double x2, double y2) {
	for (auto it = subordinates.begin(); it != subordinates.end(); ++it) {
		(*it)->move_to(random(x1, x2), random(y1, y2));
	}
	move_to(random(x1, x2), random(y1, y2));
}

void RobotCommander::free_commander_comm() {
	for (auto it = subordinates.begin(); it != subordinates.end(); ++it) {
		(*it)->free_commander();	
	}
	free_commander();
	subordinates.clear();
}