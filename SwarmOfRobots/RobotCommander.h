#pragma once
#include <list>
using std::list;

#include "RobotWorker.h"

class RobotCommander : public RobotWorker
{
private:

	// ������ ���������� �� �����������
	list<RobotWorker*> subordinates;

	double random(double min, double max);

public:
	// ����������� �� ���������
	RobotCommander();

	// ����������� ��� �����
	RobotCommander(double position_x, double position_y, double rotation);

	// �����������
	RobotCommander(const string& name, double position_x, double position_y, double rotation);

	// ������ ������ �����������
	// \new_subordinates - ���������������� �� ����� �����������
	void set_subordinates(list<RobotWorker*> new_subordinates);

	// �������� ������ �����������
	// \return ������
	list<RobotWorker*> get_subordinates();

	// ��������� ���� � ����������� ������� � ���������
	// \check_position_x1 - ����������� ���������� x1
	// \check_position_y1 - ����������� ���������� y1
	// \check_position_x2 - ����������� ���������� x2
	// \check_position_y2 - ����������� ���������� y2
	// \return true - ���� ����� ��������� � ������ ��������������
	// \return false - � ���� ������
	bool check_position_comm(double check_position_x1, double check_position_y1,
		double check_position_x2, double check_position_y2);

	// ��������� ������ � ��������� �������� �������� ��������� ��� ��������
	// \x1 - ���������� x1 ����� ����������
	// \y1 - ���������� y1 ����� ����������
	// \x2 - ���������� x2 ����� ����������
	// \y2 - ���������� y2 ����� ����������
	void move_to_comm(double x1, double y1, double x2, double y2);

	// ���������� ������ � ��� ����������� �� ������������
	void free_commander_comm();
};


