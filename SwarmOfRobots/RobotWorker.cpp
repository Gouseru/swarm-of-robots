﻿#define _USE_MATH_DEFINES
#include <iostream>
#include <assert.h>
#include <math.h>
#include "RobotWorker.h"

#define EPS 0.001

RobotWorker::RobotWorker() {
	this->name = "UnknownRobot";
	this->position_x = 0;
	this->position_y = 0;
	this->rotation  = 0;
	this->commander = nullptr;
}

RobotWorker::RobotWorker(double position_x, double position_y, double rotation)
{
	this->name = "UnknownRobot";
	this->position_x = position_x;
	this->position_y = position_y;
	this->rotation = rotation;
	this->commander = nullptr;
}

RobotWorker::RobotWorker(const string& name, double position_x, double position_y, double rotation)
{
	this->name = name;
	this->position_x = position_x;
	this->position_y = position_y;
	this->rotation = rotation;
	this->commander = nullptr;
}

void RobotWorker::set_name(const string& robot_name) {
	name = robot_name;
}

const string& RobotWorker::get_name() const {
	return name;
}

double RobotWorker::get_position_x() const {
	return position_x;
}

double RobotWorker::get_position_y() const {
	return position_y;
}

double RobotWorker::get_rotation() const {
	return rotation;
}

bool RobotWorker::check_position(double check_position_x, double check_position_y) const {
	if ((abs(position_x - check_position_x) < EPS) && (abs(position_y - check_position_y) < EPS))
		return true;
	else
		return false;
}

bool RobotWorker::check_position(double check_position_x1, double check_position_y1, 
								 double check_position_x2, double check_position_y2) const {
	if ((position_x > fmin(check_position_x1, check_position_x2)) && 
		(position_x < fmax(check_position_x1, check_position_x2)) && 
		(position_y > fmin(check_position_y1, check_position_y2)) &&
		(position_y < fmax(check_position_y1, check_position_y2)))
		return true;
	else
		return false;
}

bool RobotWorker::check_rotation(double check_rotation) const {
	if ((abs(fmod(check_rotation, 2 * M_PI) - 2 * M_PI) < EPS) or (abs(fmod(check_rotation, 2 * M_PI)) < EPS))
		return (abs(rotation - 2 * M_PI) < EPS) or (abs(rotation) < EPS);
	else
		return abs(rotation - fmod(check_rotation, 2 * M_PI)) < EPS;
}

void RobotWorker::move_on(double move) {
	position_x += move * cos(rotation);
	position_y += move * sin(rotation);
}

void RobotWorker::move_to(double move_x, double move_y) {
	position_x = move_x;
	position_y = move_y;
}

void RobotWorker::rotate_to(double move_rotation) {
	rotation = fmod(move_rotation, 2*M_PI);
}

void RobotWorker::rotate_on(double move_rotation) {
	rotation = fmod(rotation + move_rotation, 2 * M_PI);
}

void RobotWorker::set_commander(RobotCommander& new_commander) {
	commander = &new_commander;
}

const RobotCommander& RobotWorker::get_commander() {
	return *commander;
}

bool RobotWorker::check_commander(const RobotCommander& check_commander) {
	return commander == &check_commander;
}

bool RobotWorker::empty_commander() {
	return commander == nullptr;
}

void RobotWorker::free_commander() {
	commander = nullptr;
}
