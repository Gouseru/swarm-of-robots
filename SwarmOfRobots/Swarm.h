#pragma once

#include <memory>
#include <string>
#include "RobotWorker.h"
#include "RobotCommander.h"
#include "TreeKnot.h"
#include "Scenario.h"

using std::string;

class Swarm
{
private:

	// ��������� �� ������� �������
	std::shared_ptr < TreeKnot<RobotWorker> > tree_robots;
	std::shared_ptr < TreeKnot<RobotCommander> > tree_comm;

	// �������� ��������
	Scenario* scen;
	// ����� ���������� �������
	int working_worker;
	int working_comm;

public:

	// �����������
	Swarm();

	// ��������� ������ � ���
	void add_robot(const RobotWorker& new_robot);

	// ��������� ������ � ��� � ������������� �� ���������
	void add_robot();

	// ��������� ������ ��� ����� � ���
	void add_robot(double position_x, double position_y, double rotation);

	// ��������� ������ � ���
	void add_robot(const string& name, double position_x, double position_y, double rotation);

	// ��������� ������-��������� � ���
	void add_commander(const RobotCommander& new_robot);

	// ��������� ������-��������� � ��� � ������������� �� ���������
	void add_commander();

	// ��������� ������-��������� ��� ����� � ���
	void add_commander(double position_x, double position_y, double rotation);

	// ��������� ������-��������� � ���
	void add_commander(const string& name, double position_x, double position_y, double rotation);

	// ��������� �������� ��������
	void add_scenario(Scenario& scen);

	// �������� ��������� �� ������ �������
	std::shared_ptr < TreeKnot<RobotWorker> > get_robots();

	// �������� ��������� �� ������ �����������
	std::shared_ptr < TreeKnot<RobotCommander> > get_comm();

	// ��������� ��������
	void do_scenario();

	// ����������� �������
	// ��������� ��������� � �������
	RobotCommander* prepare_task(Task& task);


	// ����� ��������� ��� ��������� �������
	// ���� �� �������� � ��������
	template <typename T>
	class iterator {
		// ���������, �� �������� ����������� ������������
		TreeKnot<T>* root;
		T* rob;

	public:
		// �����������
		// \objects ������ �� ������
		// ���������� ��������� ������
		iterator(TreeKnot<T>& objects) {
			this->root = &objects;
			auto robot = this->begin();
			this->rob = robot;
		}

		// �����������
		// \objects ������ �� ������
		// \rob ������ �� ������������ ������
		iterator(TreeKnot<T>& objects, T& rob) {
			this->root = &objects;
			this->rob = &rob;
		}

		// ����� ������� ������
		// \return ��������� �� ������
		T* begin() {
			if (root->key == nullptr) {
				// c������� �������� �� ������ ������� ������
				auto it = root->objects.begin();
				if (it == root->objects.end())
					return nullptr;
				return &(*it);
			}
			else {
				// ���� �� ����� � ������� ������, ������ ��� ���� �� �������� � ��������
				return *iterator(*(root->less));
			}
		}

		// ��������� �������� �� ��������� rob �� ���������� ������
		// \return true ���� rob ��������� �� ���������� ������
		// \return false � ���� ������
		bool end() {
			if (root->key == nullptr) {
				// c������� �������� �� ������ ������� ������
				auto it = root->objects.begin();
				while (it != root->objects.end()) {
					if (&(*it) == rob)
						break;
					++it;
				}
				if (it == root->objects.end())
					return false;
				else
					return (++it) == root->objects.end();
			}
			else {
				// ���� �� ����� � ������� ������, ������ ��� ���� �� �������� � ��������
				return iterator(*(root->more), *rob).end();
			}
		}

		// �������� ������� ������� � ������
		// \return true ���� ������ ������� ���
		// \return false � ���� ������
		bool is_empty() {
			return (rob == nullptr);
		}

		// ��������� ������� �� ��������� ���������; 
		// ���� ��������� ������ ���������, ����� ����������.
		// \return ������ �� ������� �������
		T* operator*() {
			// ���� ������ �������� ���, �������� ����������.
			if (rob == nullptr) {
				char buf[50];
				sprintf_s(buf, "No more robots");
				throw std::out_of_range(buf);
			}
			// ������� �������� �� ���������� �������
			return rob;
		}

		// ����������� ��������� � ���������� �������
		// ���� ������ �������� ��� ���, ������ �� ����������.
		// � ����� ����� ����������� ���������� �������� "++"
		iterator& operator++() {
			if (rob == nullptr) {
				return *this;
			}
			// �������� ����������� ������, ����� rob ��������� �� ����� ��������� �������
			if (this->end()) {
				rob = nullptr;
				return *this;
			}
			if (!is_empty())
				rob = this->next();
			return *this;
		}

		// ������� � ���������� ������
		T* next() {
			auto temp = rob;
			if (root->key == nullptr) {
				// c������� �������� �� ������ ������� ������
				auto it = root->objects.begin();
				// ���� �������� �� ����� �� ������� ������
				while (it != root->objects.end()) {
					if (&(*it) == rob) {
						++it;
						break;
					}
					++it;
				}
				// ���� ����� ������ ��������� � �� �� ��������� � ������, �� ������� ��� �� ���������
				if (it != root->objects.end()) {
					rob = &(*it);
					return rob;
				}
				else
					return nullptr;
			}
			else {
				// ������� �������� ��������� ��� ������� �����
				temp = iterator(*(root->less), *rob).next();
				// ���� ������ ����� �� �����
				if (temp == nullptr) {
					// ��������� ��� ��� ��� �� ��������� ������� � ������� ����� 
					// ������ ������, �.�. ����� ���������� ��������� �� ������ �����
					if (iterator(*(root->less), *rob).end()) 
						temp = iterator(*(root->more), *rob).begin();
					else
						temp = iterator(*(root->more), *rob).next();
				}
				return temp;
			}
		}
	};


	// ��������� ��������� ��� ������� �� ���� �������
	// \return ��������
	iterator<RobotWorker>* take_iterator_worker() {
		return new Swarm::iterator<RobotWorker>(*tree_robots);
	}

	// ��������� ��������� ��� ������� �� ���� �����������
	// \return ��������
	iterator<RobotCommander>* take_iterator_comm() {
		return new Swarm::iterator<RobotCommander>(*tree_comm);
	}

};

