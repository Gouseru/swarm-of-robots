#include <assert.h>
#include "Swarm.h"

Swarm::Swarm() {
	tree_robots = std::make_shared<TreeKnot<RobotWorker>>();
	tree_comm = std::make_shared<TreeKnot<RobotCommander>>();
	scen = nullptr;
	working_worker = 0;
	working_comm = 0;
}

void Swarm::add_robot(const RobotWorker& new_robot) {
	tree_robots->add_object(new_robot);
}

void Swarm::add_robot() {
	tree_robots->add_object(RobotWorker());
}

void Swarm::add_robot(double position_x, double position_y, double rotation) {
	tree_robots->add_object(RobotWorker(position_x, position_y, rotation));
}

void Swarm::add_robot(const string& name, double position_x, double position_y, double rotation) {
	tree_robots->add_object(RobotWorker(name, position_x, position_y, rotation));
}

void Swarm::add_commander(const RobotCommander& new_robot) {
	tree_comm->add_object(new_robot);
}

void Swarm::add_commander() {
	tree_comm->add_object(RobotCommander());
}

void Swarm::add_commander(double position_x, double position_y, double rotation) {
	tree_comm->add_object(RobotCommander(position_x, position_y, rotation));
}

void Swarm::add_commander(const string& name, double position_x, double position_y, double rotation) {
	tree_comm->add_object(RobotCommander(name, position_x, position_y, rotation));
}

void Swarm::add_scenario(Scenario& scen) {
	this->scen = &scen;
}

std::shared_ptr < TreeKnot<RobotWorker> > Swarm::get_robots() {
	return tree_robots;
}

std::shared_ptr < TreeKnot<RobotCommander> > Swarm::get_comm() {
	return tree_comm;
}

void Swarm::do_scenario() {
	if (scen == nullptr)
		return;
	int res = 0;
	for (int i = 0;; i++) {
		res = scen->get_result(i);
		Task* task = scen->get_task(i);
		RobotCommander *comm;
		switch (res) {
		case 0:
			// Task not started
			if ((working_worker + task->get_number_of_robots() < tree_robots->get_size()) &&
				(working_comm + 1 <= tree_comm->get_size())) {
				working_worker += task->get_number_of_robots() - 1;
				working_comm++;
				comm = prepare_task(*task);
				comm->move_to_comm(task->get_x1(), task->get_y1(), task->get_x2(), task->get_y2());
				scen->set_result(i, 1);
				scen->set_time(i, 1);
			}
			else {
				scen->set_result(i, 2);
				return;
			}
			break;
		case 1:
			// Task in progress
			if (scen->get_time(i) >= scen->get_lead_time(i)) {
				comm = &(task->get_commander());
				comm->free_commander_comm();
				working_worker -= task->get_number_of_robots() - 1;
				working_comm--;
				scen->set_result(i, 5);
			}	
			else {
				scen->set_time(i, scen->get_time(i) + 1);
			}
			break;
		case 2: 
			// Task delayed
			if ((working_worker + task->get_number_of_robots() < tree_robots->get_size()) &&
				(working_comm + 1 <= tree_comm->get_size())) {
				working_worker += task->get_number_of_robots() - 1;
				working_comm++;
				comm = prepare_task(*task);
				comm->move_to_comm(task->get_x1(), task->get_y1(), task->get_x2(), task->get_y2());
				scen->set_result(i, 3);
				scen->set_time(i, 1);
			}
			else
				if (task->get_number_of_robots() > tree_robots->get_size() + 1) {
					scen->set_result(i, 4);
					return;
				}	
			break;
		case 3:
			// Task in progress after delay
			if (scen->get_time(i) >= scen->get_lead_time(i)) {
				comm = &(task->get_commander());
				comm->free_commander_comm();
				working_worker -= task->get_number_of_robots() - 1;
				working_comm--;
				scen->set_result(i, 6);
			}
			else {
				scen->set_time(i, scen->get_time(i) + 1);
			}
			break;
		case 4:
			// Task canceled
			return;
			break;
		case 5:
			// Task completed
			break;
		case 6:
			// Task completed with a delay
			break;
		case -1:
			return;
		default:
			return;
		}
		
	}
}

RobotCommander* Swarm::prepare_task(Task& task) {
	// ��������� ���������� ������
	double x1 = task.get_x1();
	double y1 = task.get_y1();
	double x2 = task.get_x2();
	double y2 = task.get_y2();
	list<RobotCommander*> comm;
	list<RobotWorker*> robots;

	// ���� �������
	// ���� �� �����, �� ����������� ������� ������
	while (comm.size() + robots.size() < task.get_number_of_robots()) {
		if (comm.size() != 1)
			comm = tree_comm->get_objects(x1, y1, x2, y2, 1);
		if (robots.size() != task.get_number_of_robots() - 1)
			robots = tree_robots->get_objects(x1, y1, x2, y2, task.get_number_of_robots() - 1);
		x1 -= 1;
		y1 -= 1;
		x2 += 1;
		y2 += 1;
	}

	task.set_commander(*(comm.front()));
	// ������������� ����������� �������������
	comm.front()->set_subordinates(robots);
	return comm.front();
}
