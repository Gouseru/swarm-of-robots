#pragma once
#include "RobotWorker.h"
#include "RobotCommander.h"

class Task
{
private:
	// ��� �������
	std::string name;
	// ����� ����������
	int lead_time;
	// ���������� ������������� ���� ����������
	double x1, y1;
	double x2, y2;
	// ��������� �� �������������� ���������
	RobotCommander* commander;
	// ����� ����������� �������
	int number_of_robots;

public:

	// ����������� �� ���������
	Task();

	// ����������� ��� �����
	// ���������� ��������� ��������
	Task(int lead_time, double x1, double y1, double x2, double y2, int number_of_robots);

	// ����������� ��� �����
	Task(int lead_time, double x1, double y1, double x2, double y2,
		int number_of_robots, RobotCommander* commander);

	// ���������� ��������� ��������
	Task(const std::string& name, int lead_time, double x1, double y1, double x2, double y2,
		int number_of_robots);

	// �����������
	Task(const std::string& name, int lead_time, double x1, double y1, double x2, double y2,
		int number_of_robots, RobotCommander* commander);

	// �������� ��� �������
	// \return ������
	const std::string& get_name() const;

	// �������� ����� ����������
	// \return int
	int get_lead_time() const;
	
	// �������� ���������� x1
	// \return int
	double get_x1() const;
	
	// �������� ���������� y1
	// \return int
	double get_y1() const;

	// �������� ���������� x2
	// \return int
	double get_x2() const;

	// �������� ���������� y2
	// \return int
	double get_y2() const;

	// �������� ������ �� �������������� ������
	// \return ������
	RobotCommander& get_commander();

	// ������ �������������� ������
	// new_comm - ������ �� �������������� ������
	void set_commander(RobotCommander& new_comm);

	// �������� ����� ����������� �������
	// \return int
	int get_number_of_robots() const;

	// ������ � �������
	void print() const {
		std::cout << "Name task: " << name << " Coordinate: From (" << x1 << ", " << y1 << ") ";
		std::cout << "To (" << x2 << ", " << y2 << ")" << " Number: " << number_of_robots << "\n";
	}
};
