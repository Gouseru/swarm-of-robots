﻿// SwarmOfRobots.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <assert.h>
#include "Robots.h"
#include "SwarmOfRobots.h"
using namespace std;

void test_robot_worker();
void test_robot_commander();
void test_tree_knot();
void test_task();
void test_scenario();
void test_swarm();

int main()
{
    std::cout << "Hello World!\n";
	test_robot_worker();
	test_robot_commander();
	test_tree_knot();
	test_swarm();
	//test_task();
	//test_scenario();
}

void test_robot_worker() {
	// Конструктор по умолчанию
	RobotWorker m0;
	assert(m0.get_position_x() == 0);
	assert(m0.get_position_y() == 0);
	assert(m0.check_position(0, 0));
	assert(m0.check_rotation(0));

	// Проверяем имя
	assert(m0.get_name() == (string)"UnknownRobot");
	assert(!(m0.get_name() == (string)""));

	// Задаем новое имя
	std::string name = "Roror";
	m0.set_name(name);
	assert(m0.get_name() == (string)"Roror");
	assert(!(m0.get_name() == (string)"UnknownRobot"));

	// Конструктор
	RobotWorker m("Robot", 5, 7, M_PI / 4);
	assert(m.get_position_x() == 5);
	assert(m.get_position_y() == 7);
	assert(m.check_position(5, 7));
	assert(m.check_rotation(M_PI / 4));
	assert(!(m.check_rotation(M_PI / 2)));

	// Поворачиваем его на пи
	m.rotate_to(M_PI);
	assert(m.check_rotation(M_PI));

	// Двигаем его
	m.move_on(2);
	assert(m.check_position(3, 7));

	// Поворачиваем его на 3/2*пи
	m.rotate_to(M_PI * 3 / 2);
	assert(m.check_rotation(M_PI * 3 / 2));

	// Двигаем его
	m.move_on(5);
	assert(m.check_position(3, 2));

	// Поворачиваем его на 2*пи
	m.rotate_to(M_PI * 1.999999);
	assert(m.check_rotation(0));

	// Задаем командира
	RobotCommander com;
	m.set_commander(com);
	assert(m.check_commander(com));
	assert(&m.get_commander() == &com);
	assert(&m.get_commander() != nullptr);

	// Освобождаем от командира
	m.free_commander();
	assert(m.empty_commander());
	assert(&m.get_commander() == nullptr);
	assert(&m.get_commander() != &com);

}

void test_robot_commander() {
	RobotCommander c;
}

void test_tree_knot() {
	// Конструктор
	TreeKnot<RobotWorker> m0;
	assert(m0.get_key() == nullptr);
	assert(m0.get_more() == nullptr);
	assert(m0.get_less() == nullptr);

	RobotWorker ob0("0", 11, 4, 2);
	RobotWorker ob1("1", 13, 14, 2);
	RobotWorker ob2("2", 7, 8, 2);
	RobotWorker ob3("3", 2, 8, 2);
	m0.add_object(ob0);
	m0.add_object(ob1);
	m0.add_object(ob2);
	m0.add_object(ob3);
}

void test_swarm() {

	Swarm m0;
	Swarm::iterator<RobotWorker>* pointer = swarm_create_iterator_worker(&m0);
	while (!swarm_iterator_is_empty_worker(pointer)) {

		std::cout << robot_get_x(swarm_iterator_get_worker(pointer)) << " ";
		swarm_iterator_next_worker(pointer);
	}
	std::cout << "\n";

	m0.add_robot("0r", 11, 4, 2);
	m0.add_robot("1r", 13, 14, 2);
	m0.add_robot("2r", 7, 8, 2);
	m0.add_robot("3r", 2, 8, 2);
	m0.add_robot("4r", 12, 3, 4);
	m0.add_robot("5r", 12, 15, 6);
	m0.add_robot("6r", 4, 3, 6);
	m0.add_robot("7r", 3, 4, 3);
	m0.add_robot("8r", 2, 4, 5);
	m0.add_robot("9r", 11, 14, 3);
	m0.add_robot("10r", 1, 3, 2);
	m0.add_robot("11r", 6, 4, 7);

	m0.add_commander("0c", 11, 4, 2);
	m0.add_commander("1c", 13, 14, 2);
	m0.add_commander("2c", 7, 8, 2);
	m0.add_commander("3c", 2, 8, 2);

	m0.get_robots()->print();
	m0.get_comm()->print();

	/*
	Scenario scen;
	scen.add_task(15, 10, 5, 15, 10, 4);
	Task t = scen.get_task(0);
	t.print();

	m0.add_scenario(scen);
	m0.do_scenario();
	m0.get_robots()->print();
	m0.get_comm()->print();
	*/
	Scenario* scen = create_scenario();
	scenario_add_task(scen, 15, 10, 5, 15, 10, 5);
	scenario_add_task(scen, 20, 11, 15, 11, 11, 4);
	scenario_add_task(scen, 5, 10, 5, 15, 10, 5);
	scenario_add_task(scen, 15, 11, 15, 11, 15, 2);
	scenario_add_task(scen, 20, 10, 5, 15, 10, 6);
	scenario_add_task(scen, 5, 11, 15, 11, 15, 5);
	scenario_add_task(scen, 20, 11, 15, 11, 15, 100);
	scenario_add_task(scen, 5, 11, 15, 11, 15, 2);
	//Task t = scen->get_task(0);
	//t.print();
	//t = scen->get_task(1);
	//t.print();

	swarm_add_scenario(&m0, scen);
	m0.get_robots()->print();
	m0.get_comm()->print();

	auto iterator1 = scenario_create_iterator(scen);
	while (!scenario_iterator_is_empty(iterator1)) {
		char res_name[200];
		auto res = scenario_iterator_get_result(iterator1);
		scenario_explain_result(scen, res, res_name, sizeof(res_name));
		cout << res_name << "\n";
		scenario_iterator_next(iterator1);

	}
	for(int i = 0; i < 20; i++)
		swarm_do_scenario(&m0);
	
	iterator1 = scenario_create_iterator(scen);
	while (!scenario_iterator_is_empty(iterator1)) {
		char res_name[200];
		auto res = scenario_iterator_get_result(iterator1);
		scenario_explain_result(scen, res, res_name, sizeof(res_name));
		cout << res_name << "\n";
		scenario_iterator_next(iterator1);

	}

	swarm_do_scenario(&m0);

	iterator1 = scenario_create_iterator(scen);
	while (!scenario_iterator_is_empty(iterator1)) {
		char res_name[200];
		auto res = scenario_iterator_get_result(iterator1);
		scenario_explain_result(scen, res, res_name, sizeof(res_name));
		cout << res_name << "\n";
		scenario_iterator_next(iterator1);

	}

	/*
	auto pointer = m0.take_iterator();
	while (!pointer->is_empty()) {
		std::cout << (**pointer)->get_name() << " ";
		++(*pointer);
	}
	std::cout << "\n";
	*/
	/*Swarm::iterator<RobotWorker>* pointer = swarm_create_iterator_worker(&m0);
	while (!swarm_iterator_is_empty_worker(pointer)) {

		std::cout << robot_get_x(swarm_iterator_get_worker(pointer)) << " ";
		swarm_iterator_next_worker(pointer);
	}
	std::cout << "\n";*/
}

void test_task() {
	Task t("Task", 15, 432, 234, 234, 123, 20);
	assert(t.get_name() == (string)"Task");
	assert(t.get_lead_time() == 15);
	assert(t.get_x1() == 432);
	assert(t.get_y1() == 234);
	assert(t.get_x2() == 234);
	assert(t.get_y2() == 123);
	assert(t.get_number_of_robots() == 20);
}

void test_scenario() {
	Scenario s;
	Task t1("Task1", 15, 432, 234, 234, 123, 2021);
	s.add_task("Task1", 15, 432, 234, 234, 123, 2021);
	s.add_task("Task2", 115, 4312, 22134, 234, 123, 220);
	s.add_task("Task3", 15, 432, 213, 432, 432, 20);
	s.add_task("Task4", 151, 123, 234, 1234, 2123, 203);

	assert((int)(s.get_task().size()) == 4);
	std::cout << s.explain_result(0) << "\n";
	std::cout << s.explain_result(1) << "\n";
	std::cout << s.explain_result(2) << "\n";
}
