#include "pch.h"
//#include "SwarmOfRobots.h"

extern "C" {
	// ������� ���
	Swarm* create_swarm() {
		return new Swarm();
	}

	// ������� ���
	void delete_swarm(Swarm* swarm) {
		if (swarm != nullptr)
			delete swarm;
	}

	// ��������� ������ � ��� � ������������� �� ���������
	void swarm_add_robot_common(Swarm* swarm) {
		swarm->add_robot();
	}

	// ��������� ������ ��� ����� � ���
	void swarm_add_robot(Swarm* swarm, int position_x, int position_y, int rotation) {
		swarm->add_robot((double)position_x, (double)position_y, (double)rotation);
	}

	// ��������� ������ � ���
	void swarm_add_robot_name(Swarm* swarm, char* name, int position_x, int position_y,
		int rotation) {
		swarm->add_robot((string)name, (double)position_x, (double)position_y, (double)rotation);
	}

	// ��������� ������-��������� � ��� � ������������� �� ���������
	void swarm_add_commander_common(Swarm* swarm) {
		swarm->add_commander();
	}

	// ��������� ������-��������� ��� ����� � ���
	void swarm_add_commander(Swarm* swarm, int position_x, int position_y, int rotation) {
		swarm->add_commander((double)position_x, (double)position_y, (double)rotation);
	}

	// ��������� ������-��������� � ���
	void swarm_add_commander_name(Swarm* swarm, char* name, int position_x, int position_y,
		int rotation) {
		swarm->add_commander((string)name, (double)position_x, (double)position_y, (double)rotation);
	}

	// ��������� �������� ��������
	void swarm_add_scenario(Swarm* swarm, Scenario* scen) {
		swarm->add_scenario(*scen);
	}

	// ��������� ��������
	void swarm_do_scenario(Swarm* swarm) {
		swarm->do_scenario();
	}

	// �����������
	Scenario* create_scenario() {
		return new Scenario();
	}

	// ������� ��������
	void delete_scenario(Scenario* scenario) {
		if (scenario != nullptr)
			delete scenario;
	}

	// �������� ������� ��� �����
	// ���������� ��������� ��������
	void scenario_add_task(Scenario* scenario, int lead_time, int x1, int y1,
		int x2, int y2, int number_of_robots) {
		scenario->add_task(lead_time, (double)x1, (double)y1, (double)x2, (double)y2, number_of_robots);
	}

	// �������� �������
	// ���������� ��������� ��������
	void scenario_add_task_name(Scenario* scenario, char* name, int lead_time,
		int x1, int y1, int x2, int y2, int number_of_robots) {
		scenario->add_task((string)name, lead_time, (double)x1, (double)y1, (double)x2, (double)y2, number_of_robots);
	}

	// �������� ��������
	Swarm::iterator<RobotWorker>* swarm_create_iterator_worker(Swarm* swarm) {
		return swarm->take_iterator_worker();
	}

	// �������� ��������
	Swarm::iterator<RobotCommander>* swarm_create_iterator_comm(Swarm* swarm) {
		return swarm->take_iterator_comm();
	}

	// ��������� ����� �� ������
	int swarm_iterator_is_empty_worker(Swarm::iterator<RobotWorker>* iterator) {
		return (int)iterator->is_empty();
	}

	// ��������� ����� �� ������
	int swarm_iterator_is_empty_comm(Swarm::iterator<RobotCommander>* iterator) {
		return (int)iterator->is_empty();
	}

	// �������� ������
	RobotWorker* swarm_iterator_get_worker(Swarm::iterator<RobotWorker>* iterator) {
		return **iterator;
	}

	// �������� ������
	RobotCommander* swarm_iterator_get_comm(Swarm::iterator<RobotCommander>* iterator) {
		return **iterator;
	}

	// �������� ��������� �� 1
	void swarm_iterator_next_worker(Swarm::iterator<RobotWorker>* iterator) {
		++(*iterator);
	}

	// �������� ��������� �� 1
	void swarm_iterator_next_comm(Swarm::iterator<RobotCommander>* iterator) {
		++(*iterator);
	}

	// �������� ��� ������
	void robot_get_name(RobotWorker* rob, char* name, int max_name_len) {
		strncpy_s(name, max_name_len, rob->get_name().c_str(), max_name_len);
	}

	// �������� ���������� x ������
	int robot_get_x(RobotWorker* rob) {
		return (int)rob->get_position_x();
	}

	// �������� ���������� y ������
	int robot_get_y(RobotWorker* rob) {
		return (int)rob->get_position_y();
	}

	// �������� ���� �������� ������
	int robot_get_rotation(RobotWorker* rob) {
		return (int)rob->get_rotation();
	}

	// 
	void robot_print(RobotWorker* rob) {
		rob->print();
	}

	// �������� ��������
	Scenario::iterator* scenario_create_iterator(Scenario* scen) {
		return scen->take_iterator_scenario();
	}

	// ��������� ����� �� ������
	int scenario_iterator_is_empty(Scenario::iterator* iterator) {
		return (int)iterator->is_empty();
	}

	// �������� �������
	Task* scenario_iterator_get_task(Scenario::iterator* iterator) {
		return **iterator;
	}

	// �������� ���������
	int scenario_iterator_get_result(Scenario::iterator* iterator) {
		return iterator->get_result();
	}

	// �������� ��������� �� 1
	void scenario_iterator_next(Scenario::iterator* iterator) {
		++(*iterator);
	}

	// �������� ���������� x ������
	void scenario_explain_result(Scenario* scen, int n, char* str, int max_str_len) {
		strncpy_s(str, max_str_len, scen->explain_result(n).c_str(), max_str_len);
	}

	// �������� ��� ������
	void task_get_name(Task* task, char* name, int max_name_len) {
		strncpy_s(name, max_name_len, task->get_name().c_str(), max_name_len);
	}

	// �������� ���������� x1
	int task_get_x1(Task* task) {
		return (int)task->get_x1();
	}

	// �������� ���������� y1
	int task_get_y1(Task* task) {
		return (int)task->get_y1();
	}

	// �������� ���������� x2
	int task_get_x2(Task* task) {
		return (int)task->get_x2();
	}

	// �������� ���������� y2
	int task_get_y2(Task* task) {
		return (int)task->get_y2();
	}

	// �������� ����� ����������
	int task_get_lead_time(Task* task) {
		return (int)task->get_lead_time();
	}

	// �������� ��������� ����������� �������
	int task_get_number_of_robots(Task* task) {
		return (int)task->get_number_of_robots();
	}
}