#pragma once
#define _USE_MATH_DEFINES
#include <iostream>
#include <assert.h>
#include <math.h>
#include <string>
#define EPS 0.001

using std::string;

class RobotCommander;

class RobotWorker
{
	friend class RobotCommander;

private:
	// ���
	string name;
	// ���������� x ������
	// ����� ���� �������������
	double position_x;
	// ���������� y ������
	// ����� ���� �������������
	double position_y;
	// ����������� / ���� �������� ������������ ��� �� [0, 2*pi)
	double rotation;
	// ��������� �� ���������
	RobotCommander* commander;

public:

	// ����������� �� ���������
	RobotWorker();

	// ����������� ��� �����
	RobotWorker(double position_x, double position_y, double rotation);

	// �����������
	RobotWorker(const string& name, double position_x, double position_y, double rotation);

	// ���������� ��� ������
	void set_name(const string& robot_name);

	// �������� ��� ������
	// \return ��� ������ � ������� string
	const string& get_name() const;

	// �������� ���������� x
	// \return �������� ���������� x
	double get_position_x() const;

	// �������� ���������� y
	// \return �������� ���������� y
	double get_position_y() const;

	// �������� ���� �������� ������������ ��� ��
	// \return �������� rotation �� ��������� [0, 2*pi)
	double get_rotation() const;

	// ��������� �������
	// \check_position_x - ����������� ���������� x
	// \check_position_y - ����������� ���������� y
	// \return true - ���� ����� ��������� � ������ �����
	// \return false - � ���� ������
	bool check_position(double check_position_x, double check_position_y) const;

	// ��������� ������� � ���������
	// \check_position_x1 - ����������� ���������� x1
	// \check_position_y1 - ����������� ���������� y1
	// \check_position_x2 - ����������� ���������� x2
	// \check_position_y2 - ����������� ���������� y2
	// \return true - ���� ����� ��������� � ������ ��������������
	// \return false - � ���� ������
	bool check_position(double check_position_x1, double check_position_y1,
		double check_position_x2, double check_position_y2) const;

	// ��������� ������� �� ����� � �������� �����������(���� ��������)
	// ����������� � ������������ ��������� �.�. �������� � double
	//\ check_rotation - ����������� ����
	// \return true - ���� ����� ������� � ��� �����������
	// \return false - � ���� ������
	bool check_rotation(double check_rotation) const;

	// ��������� ������ � ��������� ����� ��� ��������
	// \move_x - ���������� x ����� ����������
	// \move_y - ���������� y ����� ����������
	void move_to(double move_x, double move_y);

	// ��������� ������
	// \move - �����������
	void move_on(double move);
	
	// �����������(����������� � ������������ ���������)
	// ����� ������ ���� ����������� �� ���� ����
	// \move_rotation - �������� ���� mod 2*pi
	void rotate_to(double move_rotation);

	// �����������(����������� � ������������ ���������)
	// ����� �������� ���� ���������� �� ���� ����
	// \move_rotation - ��������� ����
	void rotate_on(double move_rotation);

	// ������ ���������
	void set_commander(RobotCommander& new_commander);

	// �������� ��������� �� ���������
	// \return ��������� �� ���������
	const RobotCommander& get_commander();

	// ���������, �������� �� ������ ����� ����������
	// \check_commander - ����������� �����
	// \return true - ���� ����� �������� ����������
	// \return false - � ���� ������
	bool check_commander(const RobotCommander& check_commander);

	// ���������, ��� �� ���������
	// \return true - ���� ����
	// \return false - � ���� ������
	bool empty_commander();

	// ���������� ������ �� ������������
	void free_commander();

	// ������ � �������
	void print() {
		std::cout << "Name robot: " << name << " Coordinate: (" << position_x << ", " << position_y << ")\n";
	}
};

#include "RobotCommander.h"
