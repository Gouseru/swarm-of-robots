# Импортируем библиотеку
import ctypes
import matplotlib as mpl
import matplotlib.pyplot as plt

lib = ctypes.CDLL('./SwarmOfRobots.dll')


class Interface(object):
    def __init__(self):
        # Записываем в переменную swarm указатель на рой роботов
        self.swarm = lib.create_swarm()
        # Создаем сценарий
        self.scenario = lib.create_scenario()
        # Прикрепляем сценарий к рою
        lib.swarm_add_scenario(self.swarm, self.scenario)

    def add_robot(self):
        print("Enter name, position(x, y) and rotation: ")
        line = input()
        try:
            data = line.split()
            robot_name = ctypes.c_char_p(data[0].encode('utf-8'))
            lib.swarm_add_robot_name(self.swarm, robot_name, int(data[1]), int(data[2]), int(data[3]))

        except IOError:
            print("Error has occurred!")

    def add_comm(self):
        print("Enter name, position(x, y) and rotation: ")
        line = input()
        try:
            data = line.split()
            robot_name = ctypes.c_char_p(data[0].encode('utf-8'))
            lib.swarm_add_commander_name(self.swarm, robot_name, int(data[1]), int(data[2]), int(data[3]))

        except IOError:
            print("Error has occurred!")

    def print_robots(self):
        iterator = lib.swarm_create_iterator_worker(self.swarm)
        while not lib.swarm_iterator_is_empty_worker(iterator):
            rob = lib.swarm_iterator_get_worker(iterator)

            robot_name = ctypes.create_string_buffer(100)
            lib.robot_get_name.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_size_t]
            lib.robot_get_name(rob, robot_name, ctypes.sizeof(robot_name))

            robot_x = lib.robot_get_x(rob)
            robot_y = lib.robot_get_y(rob)
            robot_rot = lib.robot_get_rotation(rob)
            print(str(robot_name.value.decode('utf-8')) + ' ' + str(robot_x)
                               + ' ' + str(robot_y) + ' ' + str(robot_rot))
            lib.swarm_iterator_next_worker(iterator)

    def print_comm(self):
        iterator = lib.swarm_create_iterator_comm(self.swarm)
        while not lib.swarm_iterator_is_empty_comm(iterator):
            rob = lib.swarm_iterator_get_comm(iterator)

            robot_name = ctypes.create_string_buffer(100)
            lib.robot_get_name.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_size_t]
            lib.robot_get_name(rob, robot_name, ctypes.sizeof(robot_name))

            robot_x = lib.robot_get_x(rob)
            robot_y = lib.robot_get_y(rob)
            robot_rot = lib.robot_get_rotation(rob)
            print(str(robot_name.value.decode('utf-8')) + ' ' + str(robot_x)
                               + ' ' + str(robot_y) + ' ' + str(robot_rot))
            lib.swarm_iterator_next_comm(iterator)

    def file_read_robots(self):
        print("Enter file name: ")
        name = input()
        try:
            with open(name) as file:
                # Выводим роботов
                for line in file:
                    data = line.split()
                    robot_name = ctypes.c_char_p(data[0].encode('utf-8'))
                    lib.swarm_add_robot_name(self.swarm, robot_name, int(data[1]), int(data[2]), int(data[3]))
        except IOError:
            print("An IOError has occurred!")

    def file_read_comm(self):
        print("Enter file name: ")
        name = input()
        try:
            with open(name) as file:
                # Выводим роботов
                for line in file:
                    data = line.split()
                    robot_name = ctypes.c_char_p(data[0].encode('utf-8'))
                    lib.swarm_add_commander_name(self.swarm, robot_name, int(data[1]), int(data[2]), int(data[3]))
        except IOError:
            print("An IOError has occurred!")

    def file_write_robots(self):
        print("Enter file name: ")
        name = input()
        try:
            with open(name, 'w') as file:
                # Выводим роботов
                iterator = lib.swarm_create_iterator_worker(self.swarm)
                while not lib.swarm_iterator_is_empty_worker(iterator):
                    rob = lib.swarm_iterator_get_worker(iterator)

                    robot_name = ctypes.create_string_buffer(100)
                    lib.robot_get_name.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_size_t]
                    lib.robot_get_name(rob, robot_name, ctypes.sizeof(robot_name))

                    robot_x = lib.robot_get_x(rob)
                    robot_y = lib.robot_get_y(rob)
                    robot_rot = lib.robot_get_rotation(rob)
                    file.write(str(robot_name.value.decode('utf-8')) + ' ' + str(robot_x)
                               + ' ' + str(robot_y) + ' ' + str(robot_rot) + '\n')
                    lib.swarm_iterator_next_worker(iterator)
        except IOError:
            print("An IOError has occurred!")

    def file_write_comm(self):
        print("Enter file name: ")
        name = input()
        try:
            with open(name, 'w') as file:
                # Выводим роботов
                iterator = lib.swarm_create_iterator_comm(self.swarm)
                while not lib.swarm_iterator_is_empty_comm(iterator):
                    rob = lib.swarm_iterator_get_comm(iterator)

                    robot_name = ctypes.create_string_buffer(100)
                    lib.robot_get_name.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_size_t]
                    lib.robot_get_name(rob, robot_name, ctypes.sizeof(robot_name))

                    robot_x = lib.robot_get_x(rob)
                    robot_y = lib.robot_get_y(rob)
                    robot_rot = lib.robot_get_rotation(rob)
                    file.write(str(robot_name.value.decode('utf-8')) + ' ' + str(robot_x)
                               + ' ' + str(robot_y) + ' ' + str(robot_rot) + '\n')
                    lib.swarm_iterator_next_comm(iterator)
        except IOError:
            print("An IOError has occurred!")

    def show_robots(self):
        robot_x = []
        robot_y = []
        iterator = lib.swarm_create_iterator_worker(self.swarm)
        while not lib.swarm_iterator_is_empty_worker(iterator):
            rob = lib.swarm_iterator_get_worker(iterator)
            robot_x.append(int(lib.robot_get_x(rob)))
            robot_y.append(int(lib.robot_get_y(rob)))
            lib.swarm_iterator_next_worker(iterator)

        comm_x = []
        comm_y = []
        iterator = lib.swarm_create_iterator_comm(self.swarm)
        while not lib.swarm_iterator_is_empty_comm(iterator):
            rob = lib.swarm_iterator_get_comm(iterator)
            comm_x.append(int(lib.robot_get_x(rob)))
            comm_y.append(int(lib.robot_get_y(rob)))
            lib.swarm_iterator_next_comm(iterator)

        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        ax.scatter(robot_x, robot_y)
        ax.scatter(comm_x, comm_y)
        plt.show()

    def clear_swarm(self):
        # Очищаем память
        lib.delete_swarm(self.swarm)
        # Записываем в переменную swarm указатель на рой роботов
        self.swarm = lib.create_swarm()

    def add_task(self):
        print("Enter name, lead time, position(x1, y1, x2, y2), number of robots: ")
        line = input()
        try:
            data = line.split()
            task_name = ctypes.c_char_p(data[0].encode('utf-8'))
            lib.scenario_add_task_name(self.scenario, task_name, int(data[1]), int(data[2]),
                                  int(data[3]), int(data[4]), int(data[5]), int(data[6]))
        except IOError:
            print("Error has occurred!")

    def print_tasks(self):
        try:
            iterator = lib.scenario_create_iterator(self.scenario)
            while not lib.scenario_iterator_is_empty(iterator):
                task = lib.scenario_iterator_get_task(iterator)

                task_name = ctypes.create_string_buffer(100)
                lib.task_get_name.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_size_t]
                lib.task_get_name(task, task_name, ctypes.sizeof(task_name))

                task_lead_time = lib.task_get_lead_time(task)
                task_x1 = lib.task_get_x1(task)
                task_y1 = lib.task_get_y1(task)
                task_x2 = lib.task_get_x2(task)
                task_y2 = lib.task_get_y2(task)
                task_number_of_robots = lib.task_get_number_of_robots(task)

                print(str(task_name.value.decode('utf-8')) + ' ' + str(task_lead_time) +
                      ' (' + str(task_x1) + ', ' + str(task_y1) + ') ' +
                      '(' + str(task_x2) + ', ' + str(task_y2) + ') ' + str(task_number_of_robots))
                lib.scenario_iterator_next(iterator)
        except IOError:
            print("Error has occurred!")

    def file_read_tasks(self):
        print("Enter file name: ")
        name = input()
        try:
            with open(name) as file:
                # Выводим роботов
                for line in file:
                    data = line.split()
                    task_name = ctypes.c_char_p(data[0].encode('utf-8'))
                    lib.scenario_add_task_name(self.scenario, task_name, int(data[1]), int(data[2]),
                                          int(data[3]), int(data[4]), int(data[5]), int(data[6]))
        except IOError:
            print("An IOError has occurred!")

    def file_write_tasks(self):
        print("Enter file name: ")
        name = input()
        try:
            with open(name, 'w') as file:
                iterator = lib.scenario_create_iterator(self.scenario)
                while not lib.scenario_iterator_is_empty(iterator):
                    task = lib.scenario_iterator_get_task(iterator)

                    task_name = ctypes.create_string_buffer(100)
                    lib.task_get_name.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_size_t]
                    lib.task_get_name(task, task_name, ctypes.sizeof(task_name))

                    task_lead_time = lib.task_get_lead_time(task)
                    task_x1 = lib.task_get_x1(task)
                    task_y1 = lib.task_get_y1(task)
                    task_x2 = lib.task_get_x2(task)
                    task_y2 = lib.task_get_y2(task)
                    task_number_of_robots = lib.task_get_number_of_robots(task)

                    file.write(str(task_name.value.decode('utf-8')) + ' ' + str(task_lead_time) +
                               ' (' + str(task_x1) + ', ' + str(task_y1) + ') ' +
                               '(' + str(task_x2) + ', ' + str(task_y2) + ') ' + str(task_number_of_robots) + '\n')
                    lib.scenario_iterator_next(iterator)

        except IOError:
            print("Error has occurred!")

    def do_scenario(self):
        try:
            print("Enter lead time: ")
            time = int(input())
            for i in range(time):
                # Выполняем сценарий
                lib.swarm_do_scenario(self.swarm)
            self.print_results()
        except IOError:
            print("Error has occurred!")

    def print_results(self):
        try:
            iterator = lib.scenario_create_iterator(self.scenario)
            while not lib.scenario_iterator_is_empty(iterator):
                res = int(lib.scenario_iterator_get_result(iterator))

                res_name = ctypes.create_string_buffer(100)
                lib.scenario_explain_result.argtypes = [ctypes.c_void_p, ctypes.c_int, ctypes.c_char_p, ctypes.c_size_t]
                lib.scenario_explain_result(self.scenario, int(res), res_name, ctypes.sizeof(res_name))

                task = lib.scenario_iterator_get_task(iterator)

                task_name = ctypes.create_string_buffer(100)
                lib.task_get_name.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_size_t]
                lib.task_get_name(task, task_name, ctypes.sizeof(task_name))

                print(str(task_name.value.decode('utf-8')), ":", str(res_name.value.decode('utf-8')))
                lib.scenario_iterator_next(iterator)
        except IOError:
            print("Error has occurred!")

    def clear_scenario(self):
        # Очищаем память
        lib.delete_scenario(self.scenario)
        # Записываем в переменную swarm указатель на рой роботов
        self.scenario = lib.create_scenario()

    def nothing(self):
        return

    def switch(self, argument):
        switcher = {
            1: self.add_robot,
            2: self.add_comm,
            3: self.print_robots,
            4: self.print_comm,
            5: self.file_read_robots,
            6: self.file_read_comm,
            7: self.file_write_robots,
            8: self.file_write_comm,
            9: self.show_robots,
            10: self.clear_swarm,
            11: self.add_task,
            12: self.print_tasks,
            13: self.file_read_tasks,
            14: self.file_write_tasks,
            15: self.do_scenario,
            16: self.print_results,
            17: self.clear_scenario,
        }
        # Get the function from switcher dictionary
        func = switcher.get(argument, self.nothing)
        # Execute the function
        func()

    def menu(self):
        while True:
            print("Menu")
            print("1: Add robot")
            print("2: Add commander")
            print("3: Print robots")
            print("4: Print commanders")
            print("5: Read robots from file")
            print("6: Read commanders from file")
            print("7: Write robots to file")
            print("8: Write commanders to file")
            print("9: Show robots using matplotlib")
            print("10: Clear swarm")

            print("11: Add task")
            print("12: Print tasks")
            print("13: Read tasks from file")
            print("14: Write tasks to file")
            print("15: Do scenario")
            print("16: Print results")
            print("17: Clear scenario")

            print("0: Exit")
            print("Enter choice: ")
            choice = int(input())
            if choice == 0:
                return
            self.switch(choice)


main = Interface()
main.menu()

main.clear_swarm()
main.clear_scenario()

